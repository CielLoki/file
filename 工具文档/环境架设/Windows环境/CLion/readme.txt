0. {Product} - product (clion, idea, etc.)
{InstallDir} - the path to the installed product (CLion, IDEA, and so on)

1. Copy JetbrainsCrack.jar to $ {InstallDir} / bin

2. Edit the file "$ {Product} .vmoptions" (or "$ {Product} $ {64} .vmoptions") in the folder {InstallDir} / bin:

2.1 Add at the end of the file (from a new line): -javaagent: {InstallDir} /bin/JetbrainsCrack.jar
for example: -javaagent:C:\Program Files\JetBrains\PyCharm 2018.1\bin\JetbrainsCrack.jar
Note: "-javaagent:C:\Program Files\..." - without space!
3. Run the product.

4. Enter key

{"licenseId":"ThisCrackLicenseId",
"licenseeName":"Rover12421",
"assigneeName":"Rover12421",
"assigneeEmail":"rover12421@163.com",
"licenseRestriction":"By Rover12421 Crack, Only Test! Please support genuine!!!",
"checkConcurrentUse":false,
"products":[
{"code":"II","paidUpTo":"2099-12-31"},
{"code":"DM","paidUpTo":"2099-12-31"},
{"code":"AC","paidUpTo":"2099-12-31"},
{"code":"RS0","paidUpTo":"2099-12-31"},
{"code":"WS","paidUpTo":"2099-12-31"},
{"code":"DPN","paidUpTo":"2099-12-31"},
{"code":"RC","paidUpTo":"2099-12-31"},
{"code":"PS","paidUpTo":"2099-12-31"},
{"code":"DC","paidUpTo":"2099-12-31"},
{"code":"RM","paidUpTo":"2099-12-31"},
{"code":"CL","paidUpTo":"2099-12-31"},
{"code":"PC","paidUpTo":"2099-12-31"},
{"code":"DB","paidUpTo":"2099-12-31"},
{"code":"GO","paidUpTo":"2099-12-31"},
{"code":"RD","paidUpTo":"2099-12-31"}
],
"hash":"2911276/0",
"gracePeriodDays":7,
"autoProlongated":false}

6. If show error msg : "Error opening zip file or JAR manifest missing : JetbrainsCrack.jar"
please modify the jar file path to absolute path in "bin/*[idea|clion|...][64].vmoptions" file.

c语言编辑器可以和vs绑定