./data/ 是存放数据库文件的路径
./src/ 是源文件文件夹
./src/main/ 主要文件夹
./src/main/java/ 各种包以及主程序入口所在根目录，是相对路径的参考点
./src/main/java/com.lin/ 项目所在包
./src/main/java/com.lin/controller/ 存放各类SpringBoot控制器
./src/main/java/com.lin/controller/front/ 存放SpringBoot前端控制器
./src/main/java/com.lin/controller/front/\*.java 用于响应各种请求的实体类
./src/main/java/com.lin/controller/manager/ 存放SpringBoot管理者控制器
./src/main/java/com.lin/controller/manager/api/ 存放api控制器
./src/main/java/com.lin/controller/manager/api/\*.java api成员控制器
./src/main/java/com.lin/controller/manager/console/ 存放各类响应请求的控制器
./src/main/java/com.lin/controller/manager/console/\*.java 各类响应控制器的实体类
./src/main/java/com.lin/controller/manager/member/ 成员控制器
./src/main/java/com.lin/controller/manager/member/\*.java 成员控制器的实体类
./src/main/java/com.lin/controller/\*.java 测试控制器的实体类
./src/main/java/com.lin/core/ 存放本项目的核心组件
./src/main/java/com.lin/core/Interceptor/ 存放各类拦截器组件
./src/main/java/com.lin/core/j2cache/ 存放Java两级缓存框架配置文件
./src/main/java/com.lin/core/j2cache/autoconfigure/
./src/main/java/com.lin/core/j2cache/cache.support/
./src/main/java/com.lin/core/redis/ redis数据库相关文件
./src/main/java/com.lin/core/security/ 安全验证核心
./src/main/java/com.lin/core/security/jwt/ json web token
./src/main/java/com.lin/core/security/\*.java 安全验证
./src/main/java/com.lin/core/shiro/ 是一个强大且易用的Java安全框架,执行身份验证、授权、密码和会话管理
./src/main/java/com.lin/core/utils/ 供核心组件使用的工具类？
./src/main/java/com.lin/core/\*.java
./src/main/java/com.lin/mapper/ 提供映射接口
./src/main/java/com.lin/mapper/console/ 映射控制台，包含各种映射器
./src/main/java/com.lin/mapper/member/ 成员映射器
./src/main/java/com.lin/model/ 用于存放各类模块的实体类
./src/main/java/com.lin/model/BaseEntity/ 基础实体类
./src/main/java/com.lin/model/BaseEntity/BaseEntity.java 用于提供继承的基础实体类
./src/main/java/com.lin/model/BaseEntity/CustomerInfo.java 用于映射客户信息
./src/main/java/com.lin/model/BaseEntity/EmailConfig.java 用于映射Email的地址
./src/main/java/com.lin/model/common/ 正常实体类，好像是用于记录数据
./src/main/java/com.lin/model/common/\*.java 数据记录实体类
./src/main/java/com.lin/model/console/ 控制台实体类，类中成员域与数据库中特定页的特定字段相对应，与./src/main/java/com.lin.mapper/console/ 中的接口相对应，但不是全部对应
./src/main/java/com.lin/model/console/\*.java 具体的控制台类
./src/main/java/com.lin/model/member/ 成员实体类，类中成员域与数据库中特定页的特定字段相对应，与./src/main/java/com.lin.mapper/member/ 中的接口相对应
./src/main/java/com.lin/model/member/\*.java 具体的成员类
./src/main/java/com.lin/service/
./src/main/java/com.lin/service/common/
./src/main/java/com.lin/service/common/\*.java Email和数据库等服务
./src/main/java/com.lin/service/console/
./src/main/java/com.lin/service/console/\*.java 控制器相关服务
./src/main/java/com.lin/service/member/\*.java 成员服务类
./src/main/java/com.lin/util/ 存放各类工具？
./src/main/java/com.lin/util/\*.java 各类工具实体类
./src/main/java/com.lin/Application 应用程序主入口
./src/main/java/com.lin/Test 应用程序测试入口

./src/main/resources/ 存放各种需要的图片、音频、视频、xml等文件的文件夹
./src/main/resources/image/ 存放图像资源
./src/main/resources/mapper/ 对应存放./src/main/java/com.lin.mapper/ 中映射类的xml配置文件
./src/main/resources/mapper/console/ 对应存放./src/main/java/com.lin.mapper/console/ 中映射控制台的xml配置文件
./src/main/resources/mapper/member/ 对应存放./src/main/java/com.lin.mapper/member/ 中映射器的xml配置文件
./src/main/resources/static/ 默认静态资源路径
./src/main/resources/templates thymeleaf解析地址的默认文件夹

./.gitignore 是存放要求git忽略的文件和文件夹的名字，使用正则表达式
./config.json 存放各种图片、音频等文件的格式、大小、路径等属性的配置
./jwt使用.md Json Web Token的使用手册
./nginx.conf Nginx的配置文件，配置内容有全局变量、事件配置、http参数、虚拟主机基本配置、Nginx状态监控、反向代理、负载均衡、URL重写、IP限制、Nginx启动、Nginx启动脚本等。Nginx是一款轻量级的Web 服务器/反向代理服务器及电子邮件（IMAP/POP3）代理服务器
./pom.xml 主要描述了项目的maven坐标，依赖关系，开发者需要遵循的规则，缺陷管理系统，组织和licenses，以及其他所有的项目相关因素，是项目级别的配置文件。


src/main/resources 各类配置文件和项目需要的图片、音频、视频等资源
JAVA反射机制是在运行状态中，对于任意一个类，都能够知道这个类的所有属性和方法；对于任意一个对象，都能够调用它的任意方法和属性；这种动态获取信息以及动态调用对象方法的功能称为java语言的反射机制。
一个正常的bean，提供默认无参构造器和若干private域以及针对每个域的set和get方法
src/test/java  测试程序
    注解开始：
    @Api value - 字段说明 description - 注释说明这个类
    @ApiModelProperty value–字段说明 name–重写属性名字 dataType–重写属性类型 required–是否必填 example–举例说明 hidden–隐藏
    @ApiOperation 按照其中的值自动生成对接口的注释，如@ApiOperation(value = “接口说明”, httpMethod = “接口请求方式”, response = “接口返回参数类型”, notes = “接口发布说明”）
    @ApiResponse code - 响应的HTTP状态码 message - 响应的信息内容
    @Autowired 它可以对类成员变量、方法及构造函数进行标注，完成自动装配的工作。
    @Basic 表示一个简单的属性到数据库表的字段的映射,对于没有任何标注的getXxxx()方法,默认 即为 @Basic
    @Bean用被注解的函数的返回值当做Bean添加入容器，在容器中的id是该方法的名字
    @CallSensitive 是JVM中专用的注解，在类加载过过程中是可以常常看到这个注解的身影的，@CallSensitive用来找到真正发起反射请求的类
    @Column 用来标识实体类中属性与数据表中字段的对应关系
    @Component 表明这是一个组件，将会被@ComponentScan发现，并被@AutoConfiguration自动配置，把普通pojo实例化到spring容器中，相当于配置文件中的 <bean id="" class=""/>
	@ComponentScan 启用组件扫描，自动添加需要的全部Spring组件。
	@Conditional 这句代码可以标注在类上面，表示该类下面的所有@Bean都会启用配置，也可以标注在方法上面，只是对该方法启用配置.使用参数值为<xxx.class extends Condition>的参数指定判定条件，符合条件的bean才会执行
	@ConditionalOnClass 将Conditional条件依赖于类
	@ConditionalOnProperty 将Conditional条件依赖于property文件
	@ConditionalOnBean 将Conditional条件依赖于Bean是否存在？
    @ConditionalOnWebApplication若是web应用环境，则该配置类文件生效
	@Configuration 启用基于Java的配置，表明这是一个配置类，用于替代Spring中的配置文件
	@ConfigurationProperties 我们想把配置文件的信息，读取并自动封装成实体类，它可以把同类的配置信息自动封装成实体类，可以用prefix属性指定全局配置文件中的特定大的字段
	@Controller 控制器Controller负责处理由DispatcherServlet分发的请求
	@Documented 注解表明这个注解应该被 javadoc工具记录. 默认情况下,javadoc是不包括注解的. 但如果声明注解时指定了 @Documented,则它会被 javadoc 之类的工具处理, 所以注解类型信息也会被包括在生成的文档中
	@EnableAutoConfiguration 启用Spring Boot的自动配置功能。使使用 @ConfigurationProperties 注解的类生效。
	@EnableCaching 注释触发一个后处理器(post processor )，它检查每个Spring bean是否存在公共方法(public method)上的缓存注释。 如果找到这样的注释，则自动创建代理以拦截方法调用并相应地处理缓存行为。
	@EnableConfigurationProperties 使使用 @ConfigurationProperties 注解的类生效。如果该类只使用了@ConfigurationProperties注解，然后该类没有在扫描路径下或者没有使用@Component等注解，导致无法被扫描为bean，那么就必须在配置类上使用@EnableConfigurationProperties注解去指定这个类，这个时候就会让该类上的@ConfigurationProperties生效，然后作为bean添加进spring容器中
	@EnableTransactionManagement 启用SpringBoot事务管理，然后在访问数据库的Service方法上添加注解 @Transactional 便可
	@Id标注用于声明一个实体类的属性映射为数据库的主键列。该属性通常置于属性声明语句之前，可与声明语句同行，也可写在单独行上
	@ImportResource导入Spring的配置文件，并让文件内容生效，在一个配置类上面使用
    @Indexed
    @JsonIgnore 使json序列化忽略当前注解的域
    @Mapper 在类上标注，指明为一个数据库映射类
    @MapperScan() 批量扫描指定路径下的mapper接口，如果不想在每个类上加@Mapper的话
	@Nullable 可以在注解的域，方法，参数上表明可以使用空指针，@NotNull代表不可以有空指针若有会编译出错，避免运行时才发现错误
	@PathVariable 接收请求路径中占位符的值
    @PropertySource 引入特定配置文件，value属性指定文件路径
	@Qualifier 注释指定注入 Bean 的名称，这样歧义就消除了
	@Repository（实现dao访问）用于标注数据访问层，也可以说用于标注数据访问组件，即DAO组件.
	@RequestBody  i) 该注解用于读取Request请求的body部分数据，使用系统默认配置的HttpMessageConverter进行解析，然后把相应的数据绑定到要返回的对象上；ii) 再把HttpMessageConverter返回的对象数据绑定到 controller中方法的参数上。
	@RequestMapping 是一个用来处理请求地址映射的注解，可用于类或方法上。用于类上，表示类中的所有响应请求的方法都是以该地址作为父路径。1、 value， method；value：     指定请求的实际地址，指定的地址可以是URI Template 模式（后面将会说明）； method：  指定请求的method类型， GET、POST、PUT、DELETE等；2、 consumes，produces； consumes： 指定处理请求的提交内容类型（Content-Type），例如application/json, text/html;produces: 指定返回的内容类型，仅当request请求头中的(Accept)类型中包含该指定类型才返回；3、 params，headers；params：指定request中必须包含某些参数值是，才让该方法处理。headers： 指定request中必须包含某些指定的header值，才能让该方法处理请求。
	@Resource   @Resource和@Autowired注解都是用来实现依赖注入的。只是@AutoWried按by type自动注入，而@Resource默认按byName自动注入。
	@ResponseBody  该注解用于将Controller的方法返回的对象，通过适当的HttpMessageConverter转换为指定格式后，写入到Response对象的body数据区
	@RestController=@Controller+@RequestBody
    @Service 服务（注入dao）用于标注服务层，主要用来进行业务的逻辑处理
	@SpringBootApplication = @Configuration + @ComponentScan + @EnableAutoConfiguration
	@Table 声明此对象映射到数据库的数据表，通过它可以为实体指定表(talbe)，name 用来命名 当前实体类 对应的数据库 表的名字 uniqueConstraints 用来批量命名唯一键
    @Transactional 注解添加到合适的方法上，并设置合适的属性信息，name当在配置文件中有多个 TransactionManager , 可以用该属性指定选择哪个事务管理器。propagation事务的传播行为，默认值为 REQUIRED。isolation事务的隔离度，默认值采用 DEFAULT。timeout 	事务的超时时间，默认值为-1。如果超过该时间限制但事务还没有完成，则自动回滚事务。read-only 	指定事务是否为只读事务，默认值为 false；为了忽略那些不需要事务的方法，比如读取数据，可以设置 read-only 为 true。rollback-for用于指定能够触发事务回滚的异常类型，如果有多个异常类型需要指定，各类型之间可以通过逗号分隔。no-rollback- for 	抛出 no-rollback-for 指定的异常类型，不回滚事务。
    @Transient 表示该属性并非一个到数据库表的字段的映射,ORM框架将忽略该属性. 如果一个属性并非数据库表的字段映射,就务必将其标示为@Transient,否则,ORM框架默认其注解为@Basic
    @Value 通过@Value将外部的值动态注入到Bean的域中，默认全局配置文件，
    注解结束
	
   1@springboot:Spring Boot是由Pivotal团队提供的全新框架，其设计目的是用来简化新Spring应用的初始搭建以及开发过程。该框架使用了特定的方式来进行配置，从而使开发人员不再需要定义样板化的配置。
   1@springmvc:Spring MVC属于SpringFrameWork的后续产品，已经融合在Spring Web Flow里面。Spring 框架提供了构建 Web 应用程序的全功能 MVC 模块。使用 Spring 可插入的 MVC 架构，从而在使用Spring进行WEB开发时，可以选择使用Spring的Spring MVC框架或集成其他MVC开发框架
   2@MyBatis:是一个基于Java的持久层框架。iBATIS提供的持久层框架包括SQL Maps和Data Access Objects（DAOs）
   2@Redis:Redis是一个开源的使用ANSI C语言编写、支持网络、可基于内存亦可持久化的日志型、Key-Value数据库，并提供多种语言的API。
    2mysql:MySQL是一个关系型数据库管理系统，由瑞典MySQL AB 公司开发，目前属于 Oracle 旗下产品。MySQL 是最流行的关系型数据库管理系统之一，在 WEB 应用方面，MySQL是最好的 RDBMS (Relational Database Management System，关系数据库管理系统) 应用软件之一。
    3shiro:Apache Shiro是一个强大且易用的Java安全框架,执行身份验证、授权、密码和会话管理。使用Shiro的易于理解的API,您可以快速、轻松地获得任何应用程序,从最小的移动应用程序到最大的网络和企业应用程序。
    4ehcache:EhCache 是一个纯Java的进程内缓存框架，具有快速、精干等特点，是Hibernate中默认的CacheProvider。
   4 j2cache:Java二级缓存框架，减少对服务器的读取数量和压力
   5@nginx:Nginx (engine x) 是一个高性能的HTTP和反向代理web服务器，同时也提供了IMAP/POP3/SMTP服务。
  5 @thymeleaf:Thymeleaf是一种用于Web和独立环境的现代服务器端的Java模板引擎。用于对html文件的多种操作
   5@html:用来提供内容
  5 @css:用于编辑内容特效位置等。CSS 能够对网页中元素位置的排版进行像素级精确控制，支持几乎所有的字体字号样式，拥有对网页对象和模型样式编辑的能力
  5 @js:用于描述行为，用来给HTML网页增加动态功能。
   5 bootstrap:基于HTML、CSS、JavaScript 开发的简洁、直观、强悍的前端开发框架，使得 Web 开发更加快捷。
  6  adminlte:AdminLTE 是一个开源的后台控制面板和仪表盘 WebApp 模板
   5 jquery:jQuery是一个快速、简洁的JavaScript框架，是继Prototype之后又一个优秀的JavaScript代码库
   
打包/部署
     Unable to find a single main class from the following candidates把对应报错文件中的main方法删掉
	#配置数据库连接
	spring.datasource.username=root	设置数据库登录名
	spring.datasource.password=123456	设置登录名对应的密码
	spring.datasource.url=jdbc:mysql://localhost/	对应的地址
	spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
	spring.datasource.schema=classpath:schema.sql	
	spring.datasource.data=classpath:data.sql
	spring.datasource.initialization-mode=always
	#mybatis映射配置文件
	mybatis.mapper-locations=classpath:mappers/mappers/*.xml

Docker操作
    安装命令:wget -qO- https://get.docker.com/ | sh
    非root用户直接运行docker:sudo usermod -aG docker
    启动docker后台服务:sudo service docker start
    docker开机启动:systemctl enable docker
    启动docker:systemctl start docker
    网易镜像加速:
    在root/etc/下创建daemon.json文件写入如下:
    {
             "registry-mirrors": ["http://hub-mirror.c.163.com"]
           }
    权限不够的话先使用sudo su进入root模式，再写入。
    搜索镜像:docker search (镜像名称)
    拉取镜像:docker pull (镜像名称):(标签，用于表示下哪个版本，可以不写，表示默认最新版)
    添加并运行:docker run --name container-name -d image-name 其中--name为自定义容器名称，参数后面跟名字，-d为后台运行，image-name指定镜像模板，这不是参数，应用镜像名字和标签（最新版需要加上latest）代替，
    列表:docker ps  查看运行中的容器，加上-a可以查看所有容器
    停止:docker stop container-name/container-id 停止表明的容器
    启动:docker start container-name/container-id 启动指定容器
    删除:docker rm container-id 删除指定容器
    tomcat需要端口映射:-p 6379:6379  举例:  eg:docker run -d -p 6379:6379 --name myredis docker.io/redis  其中-p:主机端口:容器内部端口
    mysql需要指定MYSQL_ROOT_PASSWORD、MYSQL_ALLOW_EMPTY_PASSWORD、MYSQL_RANDOM_ROOT_PASSWORD这三个参数必须指定一个才能创建:
    docker run --name (名字) -e MYSQL_ROOT_PASSWORD=（密码） -d mysql:tag
    进入容器内部sudo docker exec -it containerID /bin/bash 
    
application.properties数据源配置样例
    spring.datasource.username=root
    spring.datasource.password=123456
    spring.datasource.url=jdbc:mysql://192.168.1.103:3306/jdbc    最后的jdbc为数据库名，可变化
    spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver   自适配的驱动
    spring.datasource.schema=classpath:schema-all.sql   明确指定初始化脚本
    spring.datasource.initialization-mode=always     springboot2.0后需要带上这个配置才能使用数据库初始化脚本
    
mybatis：
    xml文件中的resultMap作用是建立数据库字段到实体类字段的映射，可以不用管什么驼峰命名法之类的问题，否则还要字段名相似等麻烦事
    
thymeleaf:
    redirect:重定向
    forward:转发

redis
    .\redis-cli -h 172.16.23.39 -p 6378  暂时不用，该参数等待研究
	redis是一种类似于Memcached的Kev-value机制的存储服务，是非关系型数据库的一种，官网www.redis.io  中文网www.redis.cn
	相对于Memcached来说，可以持久化存储数据，支持更多的数据类型，支持数据备份，主从模式
	下载redis压缩包：wget http://download.redis.io/releases/redis-5.0.4.tar.gz
	安装：redis
	1、解压 tar -zxvf redis-5.0.4.tar.gz
	2、在解压出的文件夹中安装，make install
	服务：
	安装完成之后安装文件夹下会有一个src文件夹
	其中有两个服务程序：redis-cli和redis-server
	服务测试： 输入./redis-server
	添加环境变量：
	vim /etc/profile
	写入export PATH=$PATH:(你src文件夹的路径)
	source /etc/profile使文件生效
	默认配置文件redis.conf在安装文件夹下，不在src文件夹下
    更改前记得备份配置文件
	配置文件中daemonize设置为yes使redis默认后台运行不会占用前台页面
	默认启动脚本redis_init_script在安装目录下的utils文件夹下
	编辑启动脚本文件
	EXEC=(你的redis-server路径)  服务端路径
	CLIEXEC=(你的redis-cli路径)   客户端路径
	CONF=(你的redis.conf路径)  配置文件路径
	./redis_init_script start启动脚本
	修改开启启动文件 /etc/rc.local 添加
	(你的启动脚本路径)/redis_init_script start  来自动启动redis服务
	服务测试
	查看进程 ps aux | grep redis
	关闭redis
	在src文件夹下./redis-cli 进入客户端 
	127.0.0.1:6379> SHUTDOWN  输入关闭命令
	not connected> quit   然后退出
	ps aux|grep redis 查看进程
	
SSh
    /etc/init.d/ssh restart重启ssh服务器
    vi或者vim /etc/ssh/sshd_config服务器配置，ssh_config客户端配置
    修改服务器配置文件中PasswordAuthentication 和PermitRootLogin为 yes
    Port 为端口 默认22
    ListenAddress 0.0.0.0默认侦听本机所有地址
    使用sudo passwd打开root账户密码配置页面，不配置密码 无法使用root账户远程登录
    service ssh start 开始
    service ssh stop       停止
    service ssh restart 重启
    
linux
    $ xrandr -o left   //向左旋转90度
    $ xrandr -o right //向右旋转90度
    $ xrandr -o inverted //上下翻转
    $ xrandr -o normal //回到正常角度
    大部分命令执行不了是因为没有很多工具，需要下载
    linux 命令行设置关闭盖子不休眠 （服务器架设篇）
    编辑下列文件：/etc/systemd/logind.conf        
    #HandlePowerKey按下电源键后的行为，默认power off
    #HandleSleepKey 按下挂起键后的行为，默认suspend
    #HandleHibernateKey按下休眠键后的行为，默认hibernate
    #HandleLidSwitch合上笔记本盖后的行为，默认suspend（改为lock；即合盖不休眠）在原文件中，还要去掉前面的#
    systemctl restart systemd-logind使配置生效
    Ubuntu不能使用yum，centos中使用才可以，ubuntu默认软件包管理器不是yum，而是dpkg，安装软件时用apt-get你说的网上常见法解决方法，命令改为类似:sudo apt-get install net-tools，这样就安装好了。
    出现PATH被覆盖，使用PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"直接执行。但还是要修改被修改的地方才能完全修复
    
linux文件结构
	/boot：启动目录，启动信息
	/bin：程序启动的文件，程序的命令
	/sbin：超级用户存放应用的地方
	/dev：设备目录，每个设备在linux中 都是以文件的形式存在
	/home：用户级别的目录
	/root：超级用户专属目录
	/lib：存放一些库
	/lib64：64位操作系统的库
	/lost+found：系统非正常关机时，某些无家可归的文件会在这里，默认是空的
	/media：自动识别设备时挂在这里，如DVD
	/mnt：安装临时文件系统的安装点，让用户临时挂载其他文件系统
	/proc：虚拟文件系统目录，是系统内存的映射
	/tmp：公用临时文件存储点
	/usr：存放应用程序
	/opt：存放一些可选的程序
	/sys：sysfs文件系统的挂载点
	/selinux：Security-Enhanced Linux的缩写，系统安全相关
	/srv：系统启动服务时可以访问的数据库目录
	/var：存放需要运行时需要改变的文件，比如日志文件


linux服务器搭建流程
    有root权限时可以不加sudo
    sudo apt-get update
    sudo apt-get openssh-server
    sudo
    sudo apt-get install vim
    /etc/systemd/logind.conf  设置电源键/盖子选项
    #HandlePowerKey按下电源键后的行为，默认power off
    #HandleSleepKey 按下挂起键后的行为，默认suspend
    #HandleHibernateKey按下休眠键后的行为，默认hibernate
    #HandleLidSwitch合上笔记本盖后的行为，默认suspend（改为lock；即合盖不休眠）在原文件中，还要去掉前面的#
    systemctl restart systemd-logind 重新载入
    安装g++编译器，可以通过命令“sudo apt-get install build-essential”实现。
    执行完后，就完成了gcc,g++,make的安装。build-essential是一整套工具，gcc，libc等等。
    通过“g++ -v”可以查看g++是否安装成功。

linux安装java
    创建一个目录用于存放解压后的文件，并解压缩到该目录下
    sudo mkdir /usr/lib/jvm
    sudo tar -zxvf jdk-8u181-linux-x64.tar.gz -C /usr/lib/jvm
    修改环境变量
    vim /etc/profile
    文件末尾追加如下内容
    #set oracle jdk environment
    export JAVA_HOME=/usr/local/jdk1.8.0_181
	export PATH=$PATH:$JAVA_HOME/bin:$JAVA_HOME/jre/bin:$PATH
	export CLASSPATH=.:$JAVA_HOME/lib:$JAVA_HOME/jre/lib
    使环境变量生效
    source /etc/profile 
    设置默认jdk
    sudo update-alternatives --install /usr/bin/java java /usr/lib/jvm/jdk1.8.0_181/bin/java 300  
    sudo update-alternatives --install /usr/bin/javac javac /usr/lib/jvm/jdk1.8.0_181/bin/javac 300  
    sudo update-alternatives --install /usr/bin/jar jar /usr/lib/jvm/jdk1.8.0_181/bin/jar 300   
    sudo update-alternatives --install /usr/bin/javah javah /usr/lib/jvm/jdk1.8.0_181/bin/javah 300   
    sudo update-alternatives --install /usr/bin/javap javap /usr/lib/jvm/jdk1.8.0_181/bin/javap 300
    执行
    sudo update-alternatives --config java
    测试是否安装成功
    java -version
    javac -version

linux安全模块/网络设置
	systemctl status firewalld查看防火墙状态
	systemctl stop firewalld关闭防火墙（临时）
	systemctl start firewalld开启防火墙
	安全增强型 Linux（Security-Enhanced Linux）简称 SELinux，它是一个 Linux 内核模块，也是 Linux 的一个安全子系统。
	setenforce 0关闭SELinux（临时）
	getenforce查看SELinux状态
	firewall-cmd --permanent --zone=public --add-port=3306/tcp 开启3306tcp端口，范围是public
	firewall-cmd --permanent --zone=public --add-port=3306/udp	开启3306udp端口，范围是public
	firewall-cmd --reload  reload防火墙
	设置自动连接网络
	进入/etc/sysconfig/network-scripts/  查找ifcfg-(想要连接的网络名字)类似的文件
	将ONBOOT=no 这一行，将后面的 no 改为 yes。 保存退出，重启时可自动连接

samba共享文件夹功能
	需要防火墙权限
	目前已创建一个账户用于共享
	账户名：samba
	密码：123456
	看情况关闭安全模块
	使用yum install -y samba samba-client来安装samba
	新建linux用户
	useradd 用户名
	pdbedit -a 用户名
	需要输入密码
	启动samba       service smb start
	重启samba       service smb restart
	开机启动
	chkconfig smb on
	自定义共享目录
	创建共享目录
	mkdir -p 目录地址/文件名
	修改权限
	chmod -R 777(777为全部权限) 目录地址/文件名
	修改配置文件：/etc/samba/smb.conf
	添加如下内容：
	[文件名]
	      path = (文件的路径)
	      browseable = yes     #是否可以浏览
	      writable = yes   #是否可以写入
	      public = no    #是否公开
		在windows上右键共享文件夹设置网络磁盘映射 可以不用再输ip地址了

搭建FTP服务器

从搭建FTP继续看

mariadb操作
	
	root用户 密码123456
	wutian用户 密码123456 
	systemctl status mariadb 查看状态
	systemctl start mariadb #启动服务
	systemctl enable mariadb #设置开机启动
	systemctl restart mariadb #重新启动
	systemctl stop mariadb.service #停止MariaDB
	mysql_secure_installation#初始化设置
	先是设置密码，会提示先输入密码
	Enter current password for root (enter for none):<–初次运行直接回车
	设置密码
	Set root password? [Y/n] <– 是否设置root用户密码，输入y并回车或直接回车
	New password: <– 设置root用户的密码	
	Re-enter new password: <– 再输入一次你设置的密码
	其他配置
	Remove anonymous users? [Y/n] <– 是否删除匿名用户，Y，回车
	Disallow root login remotely? [Y/n] <–是否禁止root远程登录,N，回车,
	Remove test database and access to it? [Y/n] <– 是否删除test数据库，n,回车
	Reload privilege tables now? [Y/n] <– 是否重新加载权限表，y回车
	初始化MariaDB完成，接下来测试登录
	mysql -u root -p登录

	配置mariaDB相关字符集
	vi /etc/my.cnf
	在[mysqld]下添加
	init_connect='SET collation_connection = utf8_general_ci'
	init_connect='SET NAMES utf8'
	character-set-server=utf8
	collation-server=utf8_general_ci
	skip-character-set-client-handshake

	vi /etc/my.cnf.d/client.cnf
	在[client]下添加
	default-character-set=utf8
	
	vi /etc/my.cnf.d/mysql-clients.cnf
	在[mysql]中添加
	default-character-set=utf8
	重启mariadb
	systemctl restart mariadb
	进入MariaDB查看字符集
	mysql> show variables like "%character%";show variables like "%collation%";

	创建用户命令
	mysql>create user lhy@192.168.1.10 identified by 'luxxxxxx';

	直接创建用户并授权的命令
	mysql>grant all on *.* to lhy@192.168.1.10 indentified by 'luxxxxxx';

	授予外网登陆权限 
	mysql>grant all privileges on *.* to username@'%' identified by 'password';

	授予权限并且可以授权
	mysql>grant all privileges on *.* to lhy@'192.168.1.10' identified by 'lu5896848' with grant option;

	刷新权限表
	mysql>flush privileges;

	其中只授予部分权限把 其中 all privileges或者all改为select,insert,update,delete,create,drop,index,alter,grant,references,reload,shutdown,process,file其中一部分。

	远程访问数据库
	远程访问MySQL,需开放默认端口号3306，方式有两种
	1）、centos6或更早前的版本系统
	vi /etc/sysconfig/iptables
	在
	*filter
 
	:INPUT ACCEPT [0:0]
 
	:FORWARD ACCEPT [0:0]
 
	:OUTPUT ACCEPT [0:0]
 
	-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
 
	-A INPUT -p icmp -j ACCEPT
 
	-A INPUT -i lo -j ACCEPT
 
	-A INPUT -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT
 
	-A INPUT -j REJECT --reject-with icmp-host-prohibited
 
	-A FORWARD -j REJECT --reject-with icmp-host-prohibited
 
	COMMIT
	中添加
	-A RH-Firewall-1-INPUT -m state –state NEW -m tcp -p tcp –dport 3306 -j ACCEPT

	-A RH-Firewall-1-INPUT -m state –state NEW -m udp -p udp –dport 3306 -j ACCEPT

	如果该 iptables 配置文件 不存在,先执行yum install iptables-services安装

	执行 iptables 重启生效
	service iptables restart

	2）、centos7
	执行
	firewall-cmd --permanent --zone=public --add-port=3306/tcp
 
	firewall-cmd --permanent --zone=public --add-port=3306/udp
	
	reload防火墙
	firewall-cmd
	
	