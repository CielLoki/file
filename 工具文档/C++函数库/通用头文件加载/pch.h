#ifndef PCH_H
#define PCH_H

// 添加要在此处预编译的标头
#include "qpushbutton.h"
#include "qdebug.h"
#include "iostream"
#include "qtoolbar.h"
#include "qmenubar.h"
#include "qstatusbar.h"
#include "qtextedit.h"
#include "qdockwidget.h"
#include "qdialog.h"
#include "qcolordialog.h"
#include "qfiledialog.h"
#include "qfontdialog.h"
#include "qinputdialog.h"
#include "qmessagebox.h"
#include "qdialogbuttonbox.h"
#include "qtreewidget.h"
#include "qpainter"
#include "qimagereader.h"
#include "qtimer.h"
#include "qfile.h"



#include "mylabel.h"

#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2\imgproc\types_c.h>
#include <QTextCodec>
#include "mat2qimg.h"

#endif //PCH_H
