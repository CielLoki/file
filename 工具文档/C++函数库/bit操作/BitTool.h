#pragma once

#include "qbytearray.h"
#include "qdebug.h"

class BitTool
{

public:
	static char bitSum(char x,char y)
	{
		if (x == 0)
		{
			return y;
		}
		if (y == 0)
		{
			return x;
		}
		return bitSum((x^y),((x&y)<<1));
	}


	//比特倒序
	 static char reverseBit(char bit8)
	 {
		 bit8 = ((bit8 & 0b10101010) >> 1) | ((bit8 & 0b01010101) << 1);
		 bit8 = ((bit8 & 0b11001100) >> 2) | ((bit8 & 0b00110011) << 2);
		 bit8 = ((bit8 & 0b11110000) >> 4) | ((bit8 & 0b00001111) << 4);
		 return bit8;
	 }
	 static short reverseBit(short bit16)
	 {
		 bit16 = ((bit16 & 0b1010101010101010) >> 1) | ((bit16 & 0b0101010101010101) << 1);
		 bit16 = ((bit16 & 0b1100110011001100) >> 2) | ((bit16 & 0b0011001100110011) << 2);
		 bit16 = ((bit16 & 0b1111000011110000) >> 4) | ((bit16 & 0b0000111100001111) << 4);
		 bit16 = ((bit16 & 0b1111111100000000) >> 8) | ((bit16 & 0b0000000011111111) << 8);
		 return bit16;
	 }
	 static int reverseBit(int bit32)
	 {
		 bit32 = ((bit32 & 0b10101010101010101010101010101010) >> 1) | ((bit32 & 0b01010101010101010101010101010101) << 1);
		 bit32 = ((bit32 & 0b11001100110011001100110011001100) >> 2) | ((bit32 & 0b00110011001100110011001100110011) << 2);
		 bit32 = ((bit32 & 0b11110000111100001111000011110000) >> 4) | ((bit32 & 0b00001111000011110000111100001111) << 4);
		 bit32 = ((bit32 & 0b11111111000000001111111100000000) >> 8) | ((bit32 & 0b00000000111111110000000011111111) << 8);
		 bit32 = ((bit32 & 0b11111111111111110000000000000000) >> 16) | ((bit32 & 0b00000000000000001111111111111111) << 16);
		 return bit32;
	 }

	 //浮点数转QByteArray
	 static QByteArray* floatToQByteArray(float number)
	 {
		 QByteArray* bitData = new QByteArray();
		 int x;
		 x = *(int*)&number;
		 for (int i = 3; i >= 0; i--) {
			 char temp = (x >> (i * 8)) & 0xff;
			 bitData->append(temp);
		 }
		 return bitData;
	 }

	 //交换8bit高低位
	 static char hlSwap_bit8(char bit8)
	 {
		 bit8 = ((bit8 & 0b00001111) << 4) | ((bit8 & 0b11110000) >> 4);
		 return bit8;
	 }
};

