#pragma once
#include "pch.h"
class mat2qimg {
public:
	static QImage mat_to_QImage(cv::Mat mat);
	static cv::Mat qImage_to_Mat(QImage img);
};