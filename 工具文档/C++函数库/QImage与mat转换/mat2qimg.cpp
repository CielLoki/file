#include "mat2qimg.h"
using namespace cv;
QImage mat2qimg::mat_to_QImage(cv::Mat ori) {
	/*if (ori.data == NULL) { 
		return QImage(); 
	}*/
	cv::Mat temp;
	cvtColor(ori, temp, CV_BGR2RGB);
	return 
		QImage(
		temp.data,
		temp.cols,
		temp.rows,
		temp.step,
		QImage::Format_RGB888).copy();
}

cv::Mat mat2qimg::qImage_to_Mat(QImage img) {
	if (img.isNull()) {
		return cv::Mat(); 
	}
	qDebug() << img.width()<<"   "<<img.height()<<"    "<<img.bytesPerLine();
	QImage ori =  img.convertToFormat(QImage::Format_RGB888);
	cv::Mat tar;
	cvtColor(cv::Mat(
		ori.height(),
		ori.width(),
		CV_8UC(3),
		(void *)ori.bits(),
		(uint64)ori.bytesPerLine()),
		tar, CV_RGB2BGR);
	return tar.clone();
}


