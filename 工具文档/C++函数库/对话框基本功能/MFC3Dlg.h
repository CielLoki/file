﻿
// MFC3Dlg.h: 头文件
//

#pragma once

// CMFC3Dlg 对话框
class CMFC3Dlg : public CDialogEx
{
// 构造
public:
	CMFC3Dlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFC3_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
private:
	CString m_edit;
	afx_msg void OnBnClickedOk();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton7();
	afx_msg void OnBnClickedButton8();
	afx_msg void OnBnClickedButton9();
	
private:
	void MyShowImage(cv::Mat& mat);//展示Mat格式的图片到picture control
	cv::Mat* getQianQu();
	cv::Mat* getHouJi();
	BOOL isInited();
	void updataStaticText();
	void AfterOpt(cv::Mat& mat);
	void MyLog(cv::String msg);
	void GetExtension(cv::String src, cv::String* str);
	LPCWSTR stringToLPCWSTR(std::string orig);
	void saveFileWithNoExt(CString fileName, CString filePath, CString fileExt);
};
