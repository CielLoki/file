﻿
// MFC3Dlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "MFC3.h"
#include "MFC3Dlg.h"
#include "afxdialogex.h"

#include "transformation_Image.h"
using namespace cv;
using namespace std;

vector<Mat> matList;//存储Mat的链表
int nowAt = -1;//真实的坐标


#ifdef _DEBUG
#define new DEBUG_NEW
#endif



// CMFC3Dlg 对话框



CMFC3Dlg::CMFC3Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MFC3_DIALOG, pParent)
	, m_edit(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFC3Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_edit);
}

BEGIN_MESSAGE_MAP(CMFC3Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CMFC3Dlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDOK, &CMFC3Dlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON2, &CMFC3Dlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CMFC3Dlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CMFC3Dlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CMFC3Dlg::OnBnClickedButton5)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON6, &CMFC3Dlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &CMFC3Dlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON8, &CMFC3Dlg::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON9, &CMFC3Dlg::OnBnClickedButton9)
END_MESSAGE_MAP()


// CMFC3Dlg 消息处理程序

BOOL CMFC3Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码



	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMFC3Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		
		
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMFC3Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMFC3Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}




//按钮：打开
void CMFC3Dlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	
		Mat original_image_mat;
		CString filePath2;
		GetDlgItem(IDC_EDIT1)->GetWindowText(filePath2);
		if (filePath2.IsEmpty()) {
			MessageBox(_T("先输入文件路径"));
			return;
		}
		original_image_mat = imread((String)CW2A(filePath2));
		if (original_image_mat.data == NULL) {
			MessageBox(_T("不能打开这个格式的图片"));
			return;
		}		

		matList.clear();
		matList.push_back(original_image_mat);
		nowAt = 0;
		MyShowImage(original_image_mat);
		updataStaticText();
		SetTimer(1,20,NULL);
		

}

//按钮：退出
void CMFC3Dlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnOK();
	
}

//按钮：选择
void CMFC3Dlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	TCHAR szFilter[] = _T("图像文件(*.jpg;*.png;*.bmp)|*.jpg;*.png;*.bmp|所有文件(*.*)|*.*||");
	CFileDialog fileDlg(TRUE, NULL, NULL, OFN_READONLY, szFilter, this);
	if (IDOK == fileDlg.DoModal()) {
		CString filePath1 = fileDlg.GetPathName();
		SetDlgItemText(IDC_EDIT1, filePath1);
	}

}

//按钮：黑白
void CMFC3Dlg::OnBnClickedButton3()
{
	// TODO: 在此添加控件通知处理程序代码
	if (!isInited()) { return; }
	if (nowAt >= matList.size()) { return; }
		
		Mat tar;
		if (matList[nowAt].type()==0) {
			tar = matList[nowAt];
		}
		else {
			cvtColor(matList[nowAt], tar, CV_BGR2GRAY);
		}

		AfterOpt(tar);
		
}

//draw图片
void CMFC3Dlg::MyShowImage(Mat& mat) {

	CImage cimage;//C++Image对象
	CRect m_rcShow;//C++矩形类对象
	CWnd* drawDstDlgItem = nullptr; //目标dlgitem
	CDC* pShowDC = nullptr;//C++ device context 设备上下文

	Transformation_Image::MatToCImage(mat, cimage);
	drawDstDlgItem = GetDlgItem(IDC_STATIC);;//得到输出图像的目标dlgitem
	pShowDC = drawDstDlgItem->GetDC();   //得到 IDC_STATIC 的设备上下文
	drawDstDlgItem->GetClientRect(&m_rcShow); //将 IDC_STATIC 的矩形数据提取到m_rcShow

	cimage.Draw(pShowDC->m_hDC, m_rcShow);

	drawDstDlgItem->ReleaseDC(pShowDC);
	pShowDC = nullptr;
	drawDstDlgItem = nullptr;
}


Mat* CMFC3Dlg::getQianQu()
{
	//if (!isInited) { return nullptr; }//size（）返回的是uint，当其值为0时，减一会变成最大值，即0x11111...
	if (nowAt>0 && nowAt<matList.size()) {
		nowAt--;
		return &matList[nowAt];
	}
	if (nowAt == 0) {
		MessageBox(_T("已经是第一张了"));
		return &matList[nowAt];
	}	
	return nullptr;
}

Mat* CMFC3Dlg::getHouJi()
{
	//if (!isInited) { return nullptr; }
	if (nowAt >= 0 && nowAt < matList.size()-1) {
		nowAt++;
		return &matList[nowAt];
	}
	if (nowAt == matList.size()-1) {
		MessageBox(_T("已经是最后一张了"));
		return &matList[nowAt];
	}
	return nullptr;
}

//前一张
void CMFC3Dlg::OnBnClickedButton4()
{
	// TODO: 在此添加控件通知处理程序代码
	if (!isInited()) { return; }
	MyShowImage(*getQianQu());
	updataStaticText();
}

//后一张
void CMFC3Dlg::OnBnClickedButton5()
{
	// TODO: 在此添加控件通知处理程序代码
	if (!isInited()) { return; }
	MyShowImage(*getHouJi());
	updataStaticText();
}

BOOL CMFC3Dlg::isInited() {
	if (matList.size() == 0) {
		MessageBox(_T("先打开一张图片"));
		return  FALSE;
	}
	return TRUE;
}

void CMFC3Dlg::updataStaticText() {
	CString str;
	str.Format(_T("%d/%d"), nowAt + 1, matList.size());
	GetDlgItem(IDC_TEXT)->SetWindowText(str);
}



//刷新
void CMFC3Dlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	MyShowImage(matList[nowAt]);
	CDialogEx::OnTimer(nIDEvent);
}

//平滑
void CMFC3Dlg::OnBnClickedButton6()
{
	// TODO: 在此添加控件通知处理程序代码
	if (!isInited()) { return; }
	if (nowAt >= matList.size()) { return; }
	Mat tar;
	GaussianBlur(matList[nowAt], tar, Size(3, 3), 2, 2);
	AfterOpt(tar);
}

//边缘提取
void CMFC3Dlg::OnBnClickedButton7()
{
	// TODO: 在此添加控件通知处理程序代码
	if (!isInited()) { return; }
	if (nowAt >= matList.size()) { return; }
	Mat tar,tar2;
	Laplacian(matList[nowAt], tar, CV_8UC3,3,3);
	//addWeighted(matList[nowAt], 1, tar, 0.3, 0, tar2);
	//matList[nowAt].copyTo(tar2, tar);

	AfterOpt(tar);
}

//在图像处理后的一系列相同操作
void CMFC3Dlg::AfterOpt(Mat& mat) {
	if ((nowAt + 1) < matList.size()) {
		matList.erase(matList.begin() + nowAt + 1, matList.end());
	}
	matList.push_back(mat);
	nowAt++;
	MyShowImage(mat);
	updataStaticText();

}

//图像增强
void CMFC3Dlg::OnBnClickedButton8()
{
	// TODO: 在此添加控件通知处理程序代码
	if (!isInited()) { return; }
	if (nowAt >= matList.size()) { return; }
	Mat tar, tar2;

	//增强函数写这里
	Mat kernel = (Mat_<float>(3, 3) << 0, -1, 0, 
										-1, 5, -1, 
										0, -1, 0);
	filter2D(matList[nowAt], tar, -1, kernel);
	AfterOpt(tar);

}



//另存为
void CMFC3Dlg::OnBnClickedButton9()
{
	if (!isInited()) { return; }
	TCHAR szFilter[] = _T("JPG图像(*.jpg)|*.jpg|PNG图像(*.png)|*.png|BMP图像(*.bmp)|*.bmp||");
	CFileDialog fileDlg(FALSE, _T(".jpg"), NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter, this);
	if (IDOK == fileDlg.DoModal()) {
		CString filePath1 = fileDlg.GetPathName();
		String filepath = CT2A(filePath1.GetString());
		try {
			imwrite(filepath, matList[nowAt]);
		}
		catch (Exception e) {
			CString fileExt;
			switch (fileDlg.m_ofn.nFilterIndex) {
			case 1:
				fileExt = ".jpg";
				break;
			case 2:
				fileExt = ".png";
				break;
			case 3:
				fileExt = ".bmp";
				break;
			}
			saveFileWithNoExt(fileDlg.GetFileName(), fileDlg.GetPathName(), fileExt);
		}
	}
}

//输出到日志
void CMFC3Dlg::MyLog(String msg) {
	fstream my_log;
	my_log.open("./res/log.txt",ios::app);
	my_log << msg << endl;
	my_log.close();
}

//得到后缀名
void CMFC3Dlg::GetExtension(String src,String* str) {
	int  i;
	for (i = src.length() - 1; i >= 0; i--) {
		char fengefu = src[i];
		if (fengefu == '.') {
			break;
		}
	}
	if (i == -1) {
		return;
	}	else {
			*str=src.substr(i+1, src.length());
	}
}

//String转LPCWSTR
LPCWSTR CMFC3Dlg::stringToLPCWSTR(std::string orig)
{
	size_t origsize = orig.length() + 1;
	const size_t newsize = 100;
	size_t convertedChars = 0;
	wchar_t* wcstring = (wchar_t*)malloc(sizeof(wchar_t) * (orig.length() - 1));
	mbstowcs_s(&convertedChars, wcstring, origsize, orig.c_str(), _TRUNCATE);
	return wcstring;
}

//保存没有后缀名的文件
void CMFC3Dlg::saveFileWithNoExt(CString fileName,CString filePath,CString fileExt) {
	fileName += "~linshi";
	fileName += fileExt;
	String fileName_1 = CT2A(fileName);
	imwrite(fileName_1, matList[nowAt]);
	filePath.Replace('\\', '/');
	CopyFile(fileName, filePath, FALSE);
	DeleteFile(fileName);
}